package br.edd1.finalproject;

public class SparseMatrixWithLinkedList {
    private int width;
    private final Row[] rows;

    protected class Row {
        protected Cell firstCell;
    }

    protected class Cell {
        protected int column;
        protected int value;
        protected Cell nextCell;

        public Cell(int column, int value) {
            this.column = column;
            this.value = value;
        }
    }

    public SparseMatrixWithLinkedList(int height, int width) {
        this.rows = new Row[height];
        this.width = width;
    }

    public SparseMatrixWithLinkedList(int dimension) {
        this(dimension, dimension);
    }

    public boolean coordinateIsValid(int rowIndex, int columnIndex) {
        return rowIndex < rows.length && columnIndex < width;
    }

    private boolean isEmptyMatrix(Row[] rows, int rowIndex) {
        if (rows[rowIndex] != null) {
            return false;
        }

        if (rowIndex < rows.length) {
            return isEmptyMatrix(rows, ++rowIndex);
        }

        return true;
    }

    public boolean isEmptyMatrix() {
        return isEmptyMatrix(rows, 0);
    }

    private Cell search(int columnIndex, Cell currentCell) {
        if (currentCell == null || currentCell.column > columnIndex) {
            return null;
        }

        if (currentCell.column < columnIndex) {
            return search(columnIndex, currentCell.nextCell);
        }

        return currentCell;
    }

    public Cell search(int rowIndex, int columnIndex) {
        Row row = rows[rowIndex];

        if (row == null) {
            return null;
        }

        return search(columnIndex, row.firstCell);
    }

    public Cell search(int rowIndex, int columnIndex, int value) {
        Cell cell = search(rowIndex, columnIndex);

        if (cell == null || cell.value != value) {
            return null;
        }

        return cell;
    }

    private Cell searchPrevious(int columnIndex, Cell currentCell) {
        Cell nextCell = currentCell != null ? currentCell.nextCell : null;

        if (nextCell == null || nextCell.column > columnIndex) {
            return null;
        }

        if (nextCell.column < columnIndex) {
            return searchPrevious(columnIndex, nextCell);
        }

        return currentCell;
    }

    private boolean insert(int columnIndex, int value, Cell currentCell) {
        Cell nextCell = null;
        Cell newCell = null;

        if (currentCell == null) {
            currentCell = new Cell(columnIndex, value);
        } else if (currentCell.column < columnIndex) {
            nextCell = currentCell.nextCell;

            if (nextCell != null && nextCell.column > columnIndex) {
                newCell = new Cell(columnIndex, value);

                newCell.nextCell = nextCell;
                currentCell.nextCell = newCell;
            } else {
                return insert(columnIndex, value, currentCell.nextCell);
            }
        } else {
            if (currentCell.value == value) {
                return false;
            }

            currentCell.value = value;
        }

        return true;
    }

    public boolean insert(int rowIndex, int columnIndex, int value) {
        Row row = rows[rowIndex];
        Cell firstCell = null;

        if (row == null) {
            rows[rowIndex] = new Row();
        }

        firstCell = row.firstCell;

        return insert(columnIndex, value, firstCell);
    }

    public boolean remove(int rowIndex, int columnIndex) {
        Row row = rows[rowIndex];
        Cell previousCell = null;
        Cell cell = null;
        Cell nextCell = null;

        if (row == null) {
            return false;
        }

        previousCell = searchPrevious(columnIndex, row.firstCell);

        if (previousCell == null) {
            return false;
        }

        cell = previousCell.nextCell;
        nextCell = cell.nextCell;

        previousCell.nextCell = nextCell;

        return true;
    }

    private boolean isDiagonalMatrix(Row[] rows, int rowIndex, Cell currentCell, int elementsOnDiagonal) {
        Row nextRow = rowIndex + 1 < rows.length ? rows[rowIndex + 1] : null;
        Cell firstCell = null;

        if (currentCell == null && nextRow == null) {
            return elementsOnDiagonal != 0 ? true : false;
        } else if (currentCell == null) {
            firstCell = nextRow.firstCell;

            return isDiagonalMatrix(rows, ++rowIndex, firstCell, elementsOnDiagonal);
        } else {
            if (currentCell.column > rowIndex) {
                return false;
            } else if (currentCell.column == rowIndex) {
                return isDiagonalMatrix(rows, rowIndex, currentCell.nextCell, ++elementsOnDiagonal);
            } else {
                return isDiagonalMatrix(rows, rowIndex, currentCell.nextCell, elementsOnDiagonal);
            }
        }
    }

    public boolean isDiagonalMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return isDiagonalMatrix(rows, 0, firstCell, 0);
    }

    private boolean isLineOrColumnMatrix(Row[] rows, int rowIndex, Cell currentCell, int value,
            int numberOfElements, char type) {
        Row nextRow = rowIndex + 1 < rows.length ? rows[rowIndex + 1] : null;
        Cell nextCell = null;

        if (currentCell == null && nextRow == null) {
            return numberOfElements == 0 ? true : false;
        } else if (currentCell == null) {
            nextCell = nextRow.firstCell;

            return isLineOrColumnMatrix(rows, ++rowIndex, nextCell, value, numberOfElements, type);
        } else {
            if (value == -1) {
                value = type == 'L' ? rowIndex : currentCell.column;

                return isLineOrColumnMatrix(rows, rowIndex, currentCell.nextCell, value, ++numberOfElements, type);
            } else if ((type == 'L' && rowIndex != value) || (type == 'C' && currentCell.column != value)) {
                return false;
            } else {
                return isLineOrColumnMatrix(rows, rowIndex, currentCell.nextCell, value, ++numberOfElements, type);
            }
        }
    }

    public boolean isLineMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return isLineOrColumnMatrix(rows, 0, firstCell, -1, 0, 'L');
    }

    public boolean isColumnMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return isLineOrColumnMatrix(rows, 0, firstCell, -1, 0, 'C');
    }

    private boolean isLowerOrUpperTriangularMatrix(Row[] rows, int rowIndex, Cell currentCell, int column,
            int numberOfElements, char type) {
        Row nextRow = rowIndex + 1 < rows.length ? rows[rowIndex + 1] : null;
        Cell nextCell = null;

        if (currentCell == null && nextRow == null) {
            return numberOfElements != 0 ? true : false;
        } else if (currentCell == null) {
            nextCell = nextRow.firstCell;

            return isLowerOrUpperTriangularMatrix(rows, ++rowIndex, nextCell, ++column, numberOfElements, type);
        } else {
            if ((type == 'L' && currentCell.column > column) || (type == 'U' && currentCell.column < column)) {
                return false;
            } else {
                nextCell = nextRow.firstCell;

                return isLowerOrUpperTriangularMatrix(rows, rowIndex, nextCell, column, ++numberOfElements, type);
            }
        }
    }

    public boolean isLowerTriangularMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return isLowerOrUpperTriangularMatrix(rows, 0, firstCell, 0, 0, 'L');
    }

    public boolean isUpperTriangularMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return isLowerOrUpperTriangularMatrix(rows, 0, firstCell, 0, 0, 'U');
    }

    private SparseMatrixWithLinkedList invertColumnsOfMatrix(Row[] rows, int rowIndex, Cell currentCell, int middle,
            SparseMatrixWithLinkedList newMatrix) {
        Row currentRow = rows[rowIndex];
        boolean isLastRow = rowIndex + 1 == rows.length;
        Row nextRow = !isLastRow ? rows[rowIndex + 1] : null;

        if (newMatrix == null) {
            newMatrix = new SparseMatrixWithLinkedList(rows.length);
        }

        if ((currentRow == null || currentCell == null) && isLastRow) {
            return newMatrix;
        } else if ((currentRow == null || currentCell == null) && nextRow != null) {
            return invertColumnsOfMatrix(rows, ++rowIndex, nextRow.firstCell, middle, newMatrix);
        } else if (currentRow == null || currentCell == null) {
            return invertColumnsOfMatrix(rows, ++rowIndex, null, middle, newMatrix);
        } else {
            int columnCell = currentCell.column;

            if (columnCell < middle) {
                columnCell = middle + (middle - columnCell);
            } else if (columnCell > middle) {
                columnCell = middle - (columnCell - middle);
            }

            newMatrix.insert(rowIndex, columnCell, currentCell.value);

            return invertColumnsOfMatrix(rows, rowIndex, currentCell.nextCell, middle, newMatrix);
        }
    }

    private boolean compareMatrix(Row[] rows, int rowIndex, Cell currentCell, Row[] inverseRows,
            Cell currentInverseCell) {
        boolean isLastRow = rowIndex + 1 == rows.length;
        Row currentRow = rows[rowIndex];
        Row nextRow = !isLastRow ? rows[rowIndex + 1] : null;
        Row nextInverseRow = nextRow != null ? inverseRows[rowIndex + 1] : null;

        if ((currentRow == null || currentCell == null) && !isLastRow) {
            return true;
        } else if ((currentRow == null || currentCell == null) && nextRow == null) {
            return compareMatrix(rows, ++rowIndex, null, inverseRows, null);
        } else if (currentRow == null || currentCell == null) {
            return compareMatrix(rows, ++rowIndex, nextRow.firstCell, inverseRows, nextInverseRow.firstCell);
        } else {
            if (currentCell.value == currentInverseCell.value && currentCell.column != currentInverseCell.column) {
                return false;
            } else {
                return compareMatrix(rows, rowIndex, currentCell.nextCell, inverseRows, currentInverseCell.nextCell);
            }
        }
    }

    public boolean isSymmetricMatrix() {
        int middle = rows.length % 2 == 0 ? rows.length / 2 : rows.length / 2 + 1;
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;
        SparseMatrixWithLinkedList inverseColumnsMatrix = invertColumnsOfMatrix(rows, 0, firstCell, middle, null);
        Cell firstInverseCell = rows[0] != null ? inverseColumnsMatrix.rows[0].firstCell : null;

        return compareMatrix(rows, 0, firstCell, inverseColumnsMatrix.rows, firstInverseCell);
    }

    private SparseMatrixWithLinkedList transposeMatrix(Row[] rows, int rowIndex, Cell currentCell,
            SparseMatrixWithLinkedList newMatrix) {
        boolean isLastRow = rowIndex + 1 == rows.length ? true : false;
        Row currentRow = rows[rowIndex];
        Row nextRow = !isLastRow ? rows[rowIndex + 1] : null;

        if (newMatrix == null) {
            newMatrix = new SparseMatrixWithLinkedList(rows.length, this.width);
        }

        if ((currentRow == null || currentCell == null) && isLastRow) {
            return newMatrix;
        } else if ((currentRow == null || currentCell == null) && nextRow == null) {
            return transposeMatrix(rows, ++rowIndex, null, newMatrix);
        } else if (currentRow == null || currentCell == null) {
            return transposeMatrix(rows, ++rowIndex, nextRow.firstCell, newMatrix);
        } else {
            int column = rowIndex;
            int row = currentCell.column;

            newMatrix.insert(row, column, currentCell.value);

            return transposeMatrix(rows, rowIndex, currentCell.nextCell, newMatrix);
        }
    }

    public SparseMatrixWithLinkedList transposeMatrix() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return transposeMatrix(rows, 0, firstCell, null);
    }

    private void sumCell(int rowIndex, int columnIndex, int value) {
        Cell currentCell = search(rowIndex, columnIndex);

        if (currentCell == null) {
            this.insert(rowIndex, columnIndex, value);
        } else {
            currentCell.value += value;
        }
    }

    private SparseMatrixWithLinkedList sumMatrix(Row[] rowsThis, int rowIndex, Cell currentCellThis,
            Row[] rowsOther, Cell currentCellOther, SparseMatrixWithLinkedList newMatrix) {
        Row currentRowThis = rowsThis[rowIndex];
        boolean isLastRow = rowIndex + 1 == rowsThis.length;
        Row nextRowThis = !isLastRow ? rowsThis[rowIndex + 1] : null;
        Row currentRowOther = rowsOther[rowIndex];
        Row nextRowOther = !isLastRow ? rowsOther[rowIndex + 1] : null;

        if (newMatrix == null) {
            newMatrix = new SparseMatrixWithLinkedList(rowsThis.length, this.width);
        }

        if (((currentRowThis == null && currentRowOther == null)
                || (currentCellThis == null && currentCellOther == null)) && isLastRow) {
            return newMatrix;
        } else if (((currentRowThis == null && currentRowOther == null)
                || (currentCellThis == null && currentCellOther == null)) && nextRowThis == null
                && nextRowOther == null) {
            return sumMatrix(rowsThis, ++rowIndex, null, rowsOther, null, newMatrix);
        } else if (((currentRowThis == null && currentRowOther == null)
                || (currentCellThis == null && currentCellOther == null)) && nextRowThis != null
                && nextRowOther != null) {
            return sumMatrix(rowsThis, ++rowIndex, nextRowThis.firstCell, rowsOther, nextRowOther.firstCell, newMatrix);
        } else {
            if (currentCellThis == null) {
                newMatrix.sumCell(rowIndex, currentCellOther.column, currentCellOther.value);

                return sumMatrix(rowsThis, rowIndex, currentCellThis, rowsOther, currentCellOther.nextCell, newMatrix);
            } else if (currentCellOther == null) {
                newMatrix.sumCell(rowIndex, currentCellThis.column, currentCellThis.value);

                return sumMatrix(rowsThis, rowIndex, currentCellThis.nextCell, rowsOther, currentCellOther, newMatrix);
            } else {
                newMatrix.sumCell(rowIndex, currentCellOther.column, currentCellOther.value);
                newMatrix.sumCell(rowIndex, currentCellThis.column, currentCellThis.value);

                return sumMatrix(rowsThis, rowIndex, currentCellThis.nextCell, rowsOther, currentCellOther.nextCell,
                        newMatrix);
            }
        }
    }

    public SparseMatrixWithLinkedList sumMatrix(SparseMatrixWithLinkedList otherMatrix) {
        Cell firstCellThis = rows[0] != null ? rows[0].firstCell : null;
        Cell firstCellOther = otherMatrix.rows[0] != null ? otherMatrix.rows[0].firstCell : null;

        if (this.rows.length != otherMatrix.rows.length || this.width != otherMatrix.width) {
            return null;
        }

        return sumMatrix(rows, 0, firstCellThis, otherMatrix.rows, firstCellOther, otherMatrix);
    }

    private void multiplyCell(int rowIndex, int columnIndex, int value, Row[] rows, int otherRowIndex,
            int otherColumnIndex) {
        boolean isLastRow = otherRowIndex + 1 == rows.length;
        Row currentRow = rows[otherRowIndex];
        Cell cell = null;

        if (currentRow == null && isLastRow) {
            return;
        }

        cell = search(otherRowIndex, otherColumnIndex);

        if (cell != null) {
            sumCell(rowIndex, columnIndex, value * cell.value);
        }

        multiplyCell(rowIndex, columnIndex, value, rows, ++otherRowIndex, otherColumnIndex);
    }

    private void multiplyCell(int rowIndex, Cell currentCell, SparseMatrixWithLinkedList otherMatrix) {
        Row[] rows = otherMatrix.rows;

        multiplyCell(rowIndex, currentCell.column, currentCell.value, rows, 0, rowIndex);
    }

    private SparseMatrixWithLinkedList multiplyMatrix(Row[] rowsThis, int rowIndex, Cell currentCell,
            SparseMatrixWithLinkedList otherMatrix, SparseMatrixWithLinkedList newMatrix) {
        Row currentRow = rowsThis[rowIndex];
        boolean isLastRow = rowIndex + 1 == rowsThis.length;
        Row nextRow = !isLastRow ? rowsThis[rowIndex + 1] : null;

        if (newMatrix == null) {
            newMatrix = new SparseMatrixWithLinkedList(rows.length);
        }

        if ((currentRow == null || currentCell == null) && isLastRow) {
            return newMatrix;
        } else if ((currentRow == null || currentCell == null) && nextRow == null) {
            return multiplyMatrix(rowsThis, ++rowIndex, null, otherMatrix, newMatrix);
        } else if (currentRow == null || currentCell == null) {
            return multiplyMatrix(rowsThis, ++rowIndex, nextRow.firstCell, otherMatrix, newMatrix);
        } else {
            newMatrix.multiplyCell(rowIndex, currentCell, otherMatrix);

            return multiplyMatrix(rowsThis, rowIndex, currentCell.nextCell, otherMatrix, newMatrix);
        }
    }

    public SparseMatrixWithLinkedList multiplyMatrix(SparseMatrixWithLinkedList otherMatrix) {
        Cell firstCellThis = rows[0] != null ? rows[0].firstCell : null;

        if (this.rows.length != otherMatrix.rows.length || this.width != otherMatrix.width) {
            return null;
        }

        return multiplyMatrix(rows, 0, firstCellThis, otherMatrix, null);
    }

    private String completeRow(int currentColumn, int finalColumn, String string) {
        if (string == null) {
            string = "";
        }

        if (currentColumn == 0) {
            string += "\t| 0";

            return completeRow(currentColumn + 1, finalColumn, string);
        } else {
            string += "\t0";

            if (currentColumn + 1 == finalColumn && finalColumn == width) {
                string += " |";
            } else if (currentColumn + 1 == finalColumn) {
                return string;
            } else {
                return completeRow(currentColumn + 1, finalColumn, string);
            }
        }

        return string;
    }

    private String toString(Row[] rows, int rowIndex, Cell previous, Cell currentCell, String string) {
        Row nextRow = rowIndex + 1 < rows.length ? rows[rowIndex + 1] : null;
        Cell nextCell = null;
        Cell firstCell = nextRow != null ? nextRow.firstCell : null;

        if (string == null) {
            string = "";
        }

        if (previous == null && currentCell == null) {
            string += completeRow(0, width, null);

            return nextRow == null ? string : toString(rows, ++rowIndex, null, firstCell, string + "\n");
        } else if (previous == null) {
            nextCell = currentCell.nextCell;

            string += "\t| " + currentCell.value;

            if (nextCell == null && currentCell.column + 1 != width) {
                string += completeRow(currentCell.column, width, string);

                return nextRow == null ? string : toString(rows, rowIndex, previous, currentCell, string + "\n");
            } else if (nextCell == null) {
                string += " |";

                return nextRow == null ? string : toString(rows, ++rowIndex, null, nextRow.firstCell, string + "\n");
            } else {
                if (currentCell.column + 1 != nextCell.column) {
                    string += completeRow(currentCell.column, nextCell.column + 1, string);
                }

                return toString(rows, rowIndex, currentCell, nextCell, string);
            }
        } else {
            string += "\t" + currentCell.value;

            return toString(rows, rowIndex, currentCell, currentCell.nextCell, string);
        }
    }

    @Override
    public String toString() {
        Cell firstCell = rows[0] != null ? rows[0].firstCell : null;

        return toString(rows, 0, null, firstCell, null);
    }

    public void printMatrix() {
        System.out.println(toString());
    }
}
